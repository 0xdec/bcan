EESchema Schematic File Version 4
LIBS:command_station-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L command_station-rescue:Barrel_Jack J1
U 1 1 5A2F4049
P 1500 1200
F 0 "J1" H 1500 1395 50  0000 C CNN
F 1 "PWR_IN" H 1500 1045 50  0000 C CNN
F 2 "" H 1500 1200 50  0001 C CNN
F 3 "" H 1500 1200 50  0001 C CNN
	1    1500 1200
	1    0    0    -1  
$EndComp
$Comp
L command_station-rescue:Barrel_Jack J2
U 1 1 5A2F40E9
P 1500 1700
F 0 "J2" H 1500 1895 50  0000 C CNN
F 1 "DCC_OUT" H 1500 1545 50  0000 C CNN
F 2 "" H 1500 1700 50  0001 C CNN
F 3 "" H 1500 1700 50  0001 C CNN
	1    1500 1700
	1    0    0    -1  
$EndComp
$Comp
L command_station-rescue:STM32F030C8Tx U1
U 1 1 5A301B5D
P 5700 4000
F 0 "U1" H 3400 5725 50  0000 L BNN
F 1 "STM32F030C8Tx" H 8000 5725 50  0000 R BNN
F 2 "LQFP48" H 8000 5675 50  0001 R TNN
F 3 "" H 5700 4000 50  0001 C CNN
	1    5700 4000
	1    0    0    -1  
$EndComp
$Comp
L bcan:MC33931EK U2
U 1 1 5A31B2E9
P 9850 3450
F 0 "U2" H 9850 3550 60  0000 C CNN
F 1 "MC33931EK" H 9850 3350 60  0000 C CNN
F 2 "" H 9800 3450 60  0001 C CNN
F 3 "" H 9800 3450 60  0001 C CNN
	1    9850 3450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
